import json


def read_companies_json():
    # Example usage
    companies = json.load(open('companies.json'))

    for k, v in companies.items():
        print('Symbol:', k)
        print('Name:', v['name'])
        print('Website:', v['website'])
        print()

    print(companies['AAPL'])
    print(companies['AAPL']['name'])
    print(companies['AAPL']['website'])


if __name__ == '__main__':
    read_companies_json()
